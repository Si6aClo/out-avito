# Запуск окружения
Для поднятия приложения в консоль ввести `docker compose --env-file .docker-prod-env up -d --build` или `make all`.
### Для того, чтобы разрабатывать `frontend` 
Вам нужно в консоль прописать команду `docker compose --env-file .docker-prod-env up -d frontend avito-api database redis` или `make frontend`. По ссылке `localhost:81` будет доступна апишка. По ссылке `localhost:80` будет доступен фронтенд.
### Для того, чтобы разрабатывать `backend` 
Вам нужно в консоль прописать команду `docker compose --env-file .docker-prod-env up -d database redis` или `make backend`. Теперь в IDE открываем `backend` и запускаем апишку её средствами. База данных доступна по `localhost:5432`, а redis по `localhost:6379`.
### Для того, чтобы разрабатывать `email` 
Вам нужно в консоль прописать команду `docker compose --env-file .docker-prod-env up database redis email-service --build` или `make email`. Теперь в IDE открываем `email` и запускаем приложение её средствами. База данных доступна по `localhost:5432`, а redis по `localhost:6379`.