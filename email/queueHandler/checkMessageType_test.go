package queueHandler

import "testing"

func TestUnmarshallMessage(t *testing.T) {
	t.Run("UnmarshallMessageOk", func(t *testing.T) {
		message := `{"message_type":"email","settings_id":"123"}`
		_, err := UnmarshallMessage(message)
		if err != nil {
			t.Error("Error unmarshalling message: ", err)
		}
	})
	t.Run("UnmarshallMessageErrorInName", func(t *testing.T) {
		message1 := `{"hello":"email","settings_id":"123"}`
		value1, _ := UnmarshallMessage(message1)
		if value1.MessageType == "email" && value1.SettingsId == "123" {
			t.Error("Message should not be unmarshalled")
		}

		message2 := `{"messageType":"email","settingId":"123"}`
		value2, _ := UnmarshallMessage(message2)
		if value2.MessageType == "email" && value2.SettingsId == "123" {
			t.Error("Message should not be unmarshalled")
		}
	})
}
