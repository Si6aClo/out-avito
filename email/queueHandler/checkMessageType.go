package queueHandler

import "encoding/json"

type Message struct {
	MessageType string `json:"message_type"`
	SettingsId  string `json:"settings_id"`
}

// UnmarshallMessage Unmarshall string to Message struct function and return Message struct and error
func UnmarshallMessage(message string) (Message, error) {
	var m Message
	err := json.Unmarshal([]byte(message), &m)
	if err != nil {
		return Message{}, err
	}

	return m, nil
}
