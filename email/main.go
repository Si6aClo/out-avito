package main

import (
	"context"
	"email/queueHandler"
	"email/redisClient"
	"fmt"
)

func main() {
	ctx := context.Background()
	fmt.Println("Starting email service...")
	status := redisClient.RedisClient.Ping(ctx)
	fmt.Println("Redis status: ", status)
	for {
		result, err := redisClient.RedisClient.RPop(ctx, "emails").Result()
		if err != nil {
			continue
		}

		message, err := queueHandler.UnmarshallMessage(result)
		if err != nil {
			fmt.Println("Error unmarshalling message: ", err)
			continue
		}

		fmt.Println(message)
	}
}
