all:
	docker compose --env-file .docker-prod-env up -d --build

frontend:
	docker compose --env-file .docker-prod-env up -d frontend avito-api database redis

backend:
	docker compose --env-file .docker-prod-env up -d database redis

email:
	docker compose --env-file .docker-prod-env up database redis email-service --build